import numpy as np
import cv2
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.stats import sigma_clipped_stats
from astropy.stats import sigma_clip




# create gaussian distribution
noise10 = np.random.normal(loc=1, scale=3, size=(20,20))   # centered at 10
hdu = fits.PrimaryHDU(noise10)
hdul = fits.HDUList([hdu])
hdul.writeto('noise10.fits', overwrite=True)                     # save





# create gaussian distribution
noise =  np.random.normal(loc=0., scale=3,  size=(20,20))   # centered at 0
hdu = fits.PrimaryHDU(noise)
hdul = fits.HDUList([hdu])
hdul.writeto('noise.fits', overwrite=True)                      # save




# Add the two gaussian distributions
noisy = noise10 + noise
hdu = fits.PrimaryHDU(noisy)
hdul = fits.HDUList([hdu])
hdul.writeto('noisy.fits', overwrite=True)




# Plot the histograms
# First distribution
plt.hist(noise10.ravel(), bins="auto", color="lightblue")
mean10 = np.nanmean(noise10)
std = np.nanstd(noise10)
print("normal",mean10,std)
mean, median, stddev = sigma_clipped_stats(noise10,sigma=3, cenfunc=np.nanmean, stdfunc=np.nanstd, maxiters=10)
print("sigmaclip",mean,stddev)


plt.axvline(x=mean10, color="green",label="normal mean")
plt.axvline(x=mean, color="rebeccapurple",label="sigma mean")
plt.legend()
plt.show()





# Second distribution
plt.hist(noise.ravel(), bins="auto",color="lightblue")
mean10 = np.nanmean(noise)
std = np.nanstd(noise)
print("normal",mean10,std)
mean, median, stddev = sigma_clipped_stats(noise,sigma=3, cenfunc=np.nanmean, stdfunc=np.nanstd, maxiters=10)
print("sigmaclip",mean,stddev)
plt.axvline(x=mean10, color="green",label="normal mean")
plt.axvline(x=mean, color="rebeccapurple",label="sigma mean")
plt.axvline(x=median, color="blue",label="sigma median")
plt.legend()
plt.show()



# Sum
mean10 = np.nanmean(noisy)
std = np.nanstd(noisy)
print("normal",mean10,std)
mean, median, stddev = sigma_clipped_stats(noisy,sigma=3, cenfunc=np.nanmean, stdfunc=np.nanstd, maxiters=10)
print("sigmaclip",mean,stddev)
plt.hist(noisy.ravel(), bins="auto",color="lightblue")
plt.axvline(x=mean10, color="green",label="normal mean")
plt.axvline(x=mean, color="rebeccapurple",label="sigma mean")
#plt.xlim(mean-3*std,mean+3*std)
plt.legend()
plt.show()

